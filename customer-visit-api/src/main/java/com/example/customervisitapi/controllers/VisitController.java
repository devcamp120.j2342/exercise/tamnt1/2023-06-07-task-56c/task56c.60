package com.example.customervisitapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.customervisitapi.services.VisitService;
import com.example.customervisitapi.models.models.Visit;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class VisitController {
    @Autowired
    private VisitService service;

    @GetMapping("/visits")
    public List<Visit> getAccount() {
        return service.createVisit();

    }
}
