package main.java.com.example.customervisitapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import main.java.com.example.customervisitapi.services.CustomerService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CustomerController {
    @Autowired
    private CustomerService service;

    @GetMapping("/customers")
    public List<Customer> getCustomerList() {

        return service.createCustomer();
    }
}
