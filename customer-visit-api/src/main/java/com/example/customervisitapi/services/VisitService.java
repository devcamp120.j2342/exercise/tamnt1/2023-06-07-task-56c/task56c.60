package com.example.customer.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.customervisitapi.models.models.Visit;

@Service
public class VisitService {
    public List<Visit> createVisit() {
        ArrayList<Visit> visitList = new ArrayList<>();
        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2, new Date());
        Visit visit3 = new Visit(customer3, new Date());
        visitList.addAll(Arrays.asList(visit1, visit2, visit3));
        return visitList;
    }
}
